#include <iostream>

using namespace std;

int ** Leer_Matriz (int T)
{
    int **A = (int**)malloc(T*sizeof(int*));

    cout<<"\n\n\n\t\t INGRESE MATRIZ \n";

    for ( int i=0 ; i<T ; i++)
    {
        A[i] = (int*)malloc(T*sizeof(int));
        for ( int j=0 ; j<T ; j++ )
        {
            cout<<"\n\t\t A["<<i+1<<"]["<<j+1<<"]  =  ";
            cin>>A[i][j];
        }
    }

    return A;
}

int Suma_Fila (int **M, int T)
{
    int suma2 = 0;

    for ( int i=0 ; i<T ; i++ )
    {
        int suma1 = 0;

        for (int j=0 ; j<T ; j++ )
            suma1 += M[j][i];

        if ( i == 0 )
            suma2 = suma1;
        else
            if( suma2 != suma1 )
                return -1;
    }

    return suma2;
}

int Suma_Columna (int **M, int T)
{
    int suma2 = 0;

    for ( int i=0 ; i<T ; i++ )
    {
        int suma1 = 0;

        for (int j=0 ; j<T ; j++ )
            suma1 += M[i][j];

        if ( i == 0 )
            suma2 = suma1;
        else
            if( suma2 != suma1 )
                return -1;
    }

    return suma2;
}

int Suma_Diagonal (int **M, int T)
{
    int suma1 = 0;
    int suma2 = 0;

    for ( int i=0 ; i<T ; i++ )
        suma1 += M[i][i];

    for ( int i=0 ; i<T ; i++ )
        suma2 += M[i][T-i-1];

    if( suma2 != suma1 )
        return -1;

    return suma1;
}

void Mostrar_Matriz (int **M,int T)
{
     cout<<"\n\n\n\t\t MATRIZ INGRESADA  :\n\n\n\t";
     for ( int i=0 ; i<T ; i++ )
     {
         for ( int j=0 ; j<T ; j++ )
             cout<<"\t "<<M[i][j];
         cout<<"\n\n\t";
     }
}

int main ()
{
    int **M = NULL;
    int T,V;
    char R = 'S';

    system("color 9F");

    do {
        cout<<"\n\n\n\t\t INGRESE TAMANNO DE LA MATRIZ  :  ";
        cin>>T;

        M = Leer_Matriz(T);

        Mostrar_Matriz(M,T);

        V = Suma_Fila (M,T);

        if( Suma_Columna (M,T) == V &&
           Suma_Diagonal (M,T) == V && V != -1 )

           cout<<"\n\n\n\t\t !!! LA MATRIZ ES MAGICA !!! ";

        else
           cout<<"\n\n\n\t\t !!! LA MATRIZ NO ES MAGICA !!! ";

        cout<<"\n\n\n\t\t DESEA CONTINUAR [S/N]  :  ";
        cin>>R;

        } while ( R == 'S' || R == 's' );

    return 0;
}
