#include <iostream>
using namespace std;
void fun_a(int *px, int *py);
void fun_b(int a[], int tam);
int main()
{
    //int i;
int array[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; //direccion de array 0x7ffc0f639020
//for (i=1;i<=10;i++)
  //  cout<<"Este valor ocupa "<<sizeof (array[i])<<" bytes"<<endl; // imprime el valor en bites de cada entrada del array
fun_b(array, 10);
}
void fun_a(int *px, int *py){
int tmp = *px;
*px = *py;
*py = tmp;
}
void fun_b(int a[], int tam){
int f, l;
int *b = a;
for (f = 0, l = tam -1; f < l; f++, l--) {
fun_a(&b[f], &b[l]);
}
}
